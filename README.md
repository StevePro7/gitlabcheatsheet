# GitLab Cheat Sheet
Thu, 4th July, 2024
<br />
Instructions for GitLab Cheat Sheet blog post
<br />
https://steveproxna.blogspot.com/2024/07/gitlab-cheat-sheet.html
<br /><br />
Pre-Requisites
```
Python 3.8      [python]
GitLab account  [CI/CD]
Microsoft Azure subscription
VS Code Docker + Kubernetes 
az Command Line Interface
Docker Hub account
Kubernetes in Docker
docker
kubectl
```
LocalHost
```
pip install -r requirements.txt
pip install --upgrade pip
touch main.py
python main.py
curl http://localhost:8080
sudo fuser -k 8080/tcp
```
 Docker
```
docker build --pull --rm -f "Dockerfile" -t flask-api:latest "."
docker run --rm -d -p 8080:8080/tcp flask-api:latest --name "flask-api"
docker logs -f <conteinar_id>
curl http://localhost:8080
docker stop <conteinar_id>
```
Kubernetes [local]
```
kind create cluster --name flask-cluster
kubectl create ns test-ns
kubectl config set-context --current --namespace=test-ns
kind load docker-image flask-api:latest --name flask-cluster
kubectl apply -f Kubernetes.yaml
kubectl logs -f pod/flask-api-deployment-<pod_id>
kubectl port-forward service/flask-api-service 8080:80
curl http://localhost:8080
kubectl delete -f Kubernetes.yaml
kind delete cluster --name flask-cluster
```
Kubernetes [remote]
```
az login
az ad sp create-for-rbac --name ${USER}-bz-sp

cd ~/.ssh
ssh-keygen -t rsa -b 4096 -N '' -f master_ssh_key
eval $(ssh-agent -s)
ssh-add master_ssh_key

export AZ_SP_ID=<value_from_appId>
export AZ_SP_PASSWORD=<value_from_password>
export AZ_TENANT_ID=<value_from_tenant>
export AZ_CREATE_MODE=AKS
export AZ_NETWORK_MODE=transparent
export AZ_LB_SKU=standard
export AZ_VM_SIZE=Standard_D2s_v3
export CLUSTER_NODES=3
export KUBERNETES_VERSION=1.28
export MASTER_CONNECT_KEY_PUB=~/.ssh/master_ssh_key.pub
export CLUSTER_NAME=stevepro-dev-cluster
export AZ_LOCATION=northeurope

az group create --name ${CLUSTER_NAME} --location ${AZ_LOCATION} --debug
az aks create --name ${CLUSTER_NAME} --resource-group ${CLUSTER_NAME} --dns-name-prefix ${CLUSTER_NAME} \
    --node-count ${CLUSTER_NODES} --node-vm-size ${AZ_VM_SIZE} --kubernetes-version ${KUBERNETES_VERSION} \
    --ssh-key-value ${MASTER_CONNECT_KEY_PUB} --service-principal ${AZ_SP_ID} --client-secret ${AZ_SP_PASSWORD} \
    --load-balancer-sku ${AZ_LB_SKU} --network-plugin azure

export KUBECONFIG=~/.kube/config
az aks get-credentials --debug --name ${CLUSTER_NAME} --resource-group ${CLUSTER_NAME} --file ${KUBECONFIG}
kubectl create ns test-ns
kubectl config set-context --current --namespace=test-ns

# CI/CD pipeline variables
kubectl port-forward service/flask-api-service 8080:80
curl http://localhost:8080
```